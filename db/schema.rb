# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_12_10_145238) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "answers", force: :cascade do |t|
    t.string "answer"
    t.bigint "question_choice_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["question_choice_id"], name: "index_answers_on_question_choice_id"
  end

  create_table "categories", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "creators", force: :cascade do |t|
    t.string "email"
    t.string "name"
    t.string "surname"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "password_digest"
    t.index ["email"], name: "index_creators_on_email", unique: true
  end

  create_table "question_choice_questions", force: :cascade do |t|
    t.bigint "question_choice_id", null: false
    t.bigint "question_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["question_choice_id"], name: "index_question_choice_questions_on_question_choice_id"
    t.index ["question_id"], name: "index_question_choice_questions_on_question_id"
  end

  create_table "question_choices", force: :cascade do |t|
    t.string "choice"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "question_surveys", force: :cascade do |t|
    t.bigint "question_id", null: false
    t.bigint "survey_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["question_id"], name: "index_question_surveys_on_question_id"
    t.index ["survey_id"], name: "index_question_surveys_on_survey_id"
  end

  create_table "questions", force: :cascade do |t|
    t.string "question"
    t.integer "question_type"
    t.boolean "profile_question"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "survey_categories", force: :cascade do |t|
    t.bigint "category_id", null: false
    t.bigint "survey_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["category_id"], name: "index_survey_categories_on_category_id"
    t.index ["survey_id"], name: "index_survey_categories_on_survey_id"
  end

  create_table "surveys", force: :cascade do |t|
    t.string "title"
    t.text "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "creator_id", null: false
    t.boolean "public", default: true
    t.index ["creator_id"], name: "index_surveys_on_creator_id"
  end

  add_foreign_key "answers", "question_choices"
  add_foreign_key "question_choice_questions", "question_choices"
  add_foreign_key "question_choice_questions", "questions"
  add_foreign_key "question_surveys", "questions"
  add_foreign_key "question_surveys", "surveys"
  add_foreign_key "survey_categories", "categories"
  add_foreign_key "survey_categories", "surveys"
  add_foreign_key "surveys", "creators"
end
