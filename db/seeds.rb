# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Creator.create!(email: "andrea02.fortini@student.unife.it", 
  name: "Andrea", 
  surname: "Fortini", 
  password: "andrea", 
  password_confirmation: "andrea")

  creator = Creator.first

creator.surveys.create!(title: "Sondaggio di Prova",
  description: "Lore Ipsum sarà la descrizione di test")

40.times do |n|
  title = Faker::Book.title
  description = Faker::ChuckNorris.fact
  creator.surveys.create!(title: title,
                          description: description,
                          public: true)
end
