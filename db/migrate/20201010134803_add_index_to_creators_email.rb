class AddIndexToCreatorsEmail < ActiveRecord::Migration[6.0]
  def change
    add_index :creators, :email, unique: true
  end
end
