class CreateQuestions < ActiveRecord::Migration[6.0]
  def change
    create_table :questions do |t|
      t.string :question
      t.integer :type
      t.boolean :profile_question
      t.references :survey, null: false, foreign_key: true

      t.timestamps
    end
  end
end
