class RemoveQuestionFromQuestionChoice < ActiveRecord::Migration[6.0]
  def change
    remove_column :question_choices, :question_id
  end
end
