class AddPasswordDigestToCreators < ActiveRecord::Migration[6.0]
  def change
    add_column :creators, :password_digest, :string
  end
end
