class CreateQuestionChoices < ActiveRecord::Migration[6.0]
  def change
    create_table :question_choices do |t|
      t.string :choice
      t.references :question, null: false, foreign_key: true

      t.timestamps
    end

  end
end
