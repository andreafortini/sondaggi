class MigrateRelationRecordsFromQuestion < ActiveRecord::Migration[6.0]
  def up
    Question.all.each do |question| 
      QuestionSurvey.create(question_id: question.id, survey_id: question.survey_id) 
    end
  end
end
