class AddPublicToSurveys < ActiveRecord::Migration[6.0]
  def change
    add_column :surveys, :public, :boolean, default: true
  end
end
