class RemoveSurveyFromQuestion < ActiveRecord::Migration[6.0]
  def change
    remove_column :questions, :survey_id
  end
end
