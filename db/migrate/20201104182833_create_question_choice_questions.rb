class CreateQuestionChoiceQuestions < ActiveRecord::Migration[6.0]
  def change
    create_table :question_choice_questions do |t|
      t.references :question_choice, null: false, foreign_key: true
      t.references :question, null: false, foreign_key: true

      t.timestamps
    end
  end
end
