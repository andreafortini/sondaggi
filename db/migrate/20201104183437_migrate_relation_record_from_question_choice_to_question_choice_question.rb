class MigrateRelationRecordFromQuestionChoiceToQuestionChoiceQuestion < ActiveRecord::Migration[6.0]
  def up
    QuestionChoice.all.each do |choice| 
      QuestionChoiceQuestion.create(question_choice_id: choice.id, question_id: choice.question_id) 
    end
  end
end
