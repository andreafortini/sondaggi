# Survey web application

Survey is a Rails web application where you can answer and create questionnaires. You could also register in the platform and start to create your own surveys private or pubblic.

## Dependencies

Before start you should have in your machine:
* [Ruby](https://www.ruby-lang.org/en/downloads/) 2.7.2
* [Yarn](https://classic.yarnpkg.com/en/docs/install/#debian-stable) 1.22.5
* [PostegreSQL](https://www.postgresql.org/download/) 12.5
* [Node.js](https://nodejs.org/en/download/) 10.19

Check the [Documentation](https://guides.rubyonrails.org/getting_started.html) on Ruby on Rails' site to have a step by step istruction of how to start.

## Installation
Clone the repository in your directory and install all the gems that has required.
First of all you have to install Bundler and then install all the gems:
```sh
$ gem install bundler
$ bundle install
```
You have to set up your database (you can follow [this guide](https://www.postgresql.org/docs/9.0/tutorial-createdb.html)).

Run the server:
```sh
$ rails server
```