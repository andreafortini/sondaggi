class SurveyCategory < ApplicationRecord
  belongs_to :category
  belongs_to :survey

  validates :category_id, presence: true
  validates :survey_id, presence: true
end
