class Creator < ApplicationRecord
  has_many :surveys, dependent: :destroy
  
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i

  before_save { self.email = email.downcase }

  validates :email, 
        presence: true, 
        length: { maximum: 255 }, 
        format: { with: VALID_EMAIL_REGEX }, 
        uniqueness: { case_sensitive: false } 

  validates :name, 
        presence: true, 
        length: { maximum: 50 }

  validates :surname, 
        presence: true, 
        length: { maximum: 50 }

  has_secure_password
  validates :password, 
        length: { minimum: 6 },
        allow_blank: true


  # Returns the hash digest of the given string.
  def Creator.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end
end
