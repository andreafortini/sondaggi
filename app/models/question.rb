class Question < ApplicationRecord
  has_many :question_surveys
  has_many :surveys, through: :question_surveys, dependent: :destroy
  has_many :question_choice_questions
  has_many :question_choices, through: :question_choice_questions, dependent: :destroy

  validates :profile_question, 
    inclusion: { in: [true, false] }

  validates :question, 
    presence: true,  
    length: { maximum: 255 }

  validates :question_type, 
    presence: true, 
    inclusion: { in: [1, 2, 3] }
  
  # This query is necessary to discover the number of answers. The only sure value of how many compiled form has done
  # is how many radio button or text field has compiled. In order to discover that we have to extrat from db the first
  # avaiable radio button or text question_id to execut th others query.
  # NOTE: this query returns ALL radio or text question, you have to pass only one to other query
  scope :question_radio_or_text_of_survey, -> (survey_id) {
    joins(:surveys)
    .where("question_surveys.survey_id = ?", survey_id)
    .where("questions.question_type = ? or questions.question_type = ?", 1, 2)
  }

  def self.find_or_init(params, creator)
    question = Question.joins(:surveys).where("creator_id = #{creator.id}")
                       .find_by(question: params[:question], question_type: params[:question_type], profile_question: params[:profile_question])
    if question.blank?
      # create a new record
      question = Question.new(question: params[:question], question_type: params[:question_type], profile_question: params[:profile_question])
    else
      # return the record
      return question
    end
  end
    
end
