class QuestionChoiceQuestion < ApplicationRecord
  belongs_to :question_choice
  belongs_to :question

  validates :question_choice_id, presence: true
  validates :question_id, presence: true
end
