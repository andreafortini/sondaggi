class QuestionChoice < ApplicationRecord
  has_many :question_choice_questions
  has_many :questions, through: :question_choice_questions, dependent: :destroy
  has_one :answer, dependent: :destroy
 
  validates :choice,
    length: { maximum: 140 }

  def self.find_or_init(params, creator)
    question_choice = QuestionChoice.joins(questions: :surveys).where("creator_id = #{creator.id}").find_by(choice: params[:choice])
    if question_choice.blank?
      # create a new record
      question_choice = QuestionChoice.new(choice: params[:choice])
    else
      # return the record
      return question_choice
    end
  end
end
