class QuestionSurvey < ApplicationRecord
  belongs_to :question
  belongs_to :survey

  validates :question_id, presence: true
  validates :survey_id, presence: true
end
