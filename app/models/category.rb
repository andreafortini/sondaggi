class Category < ApplicationRecord
  has_many :survey_categories
  has_many :surveys, through: :survey_categories, dependent: :destroy

  before_save do
    self.name = name.downcase
    self.name = name.parameterize
  end

  validates :name, presence: true, length: { maximum: 20 }, uniqueness: { case_sensitive: false }

  def self.find_or_create(params)
    name = params[:name].downcase.parameterize
    category = Category.find_by(name: name)
    if category.blank?
      # create a new record
      category = Category.new(name: name)
    else
      # return the record
      return category
    end
  end

end
