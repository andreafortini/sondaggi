class Answer < ApplicationRecord
  belongs_to :question_choice

  validates :question_choice_id,
    presence: true
  validates :answer,
    presence: true,
    length: { maximum: 140 }

  scope :count_answers_of_survey, -> (question_id) {
    joins(question_choice: :questions)
    .where("question_choice_questions.question_id = ?", question_id)
    .count
  }

  scope :count_answers_of_question_choice, -> (survey_id, question_choice_id) {
    joins(question_choice: [questions: :surveys])
    .where("question_surveys.survey_id = ?", survey_id)
    .where("question_choices.id = ?", question_choice_id)
    .count
  }

  scope :answers_of_text_field, -> (survey_id, question_choice_id) {
    joins(question_choice: [questions: :surveys])
    .where("question_surveys.survey_id = ?", survey_id)
    .where("question_choices.id = ?", question_choice_id)
    .select(:answer)
    .distinct
  }
  
  def self.set_answers(questionnaire)

    answers_array = []
    answers = []

    questionnaire.each do |type, content|
      case type
      when "text_fields"
        content.each do |c|
          c.each do |key,value|
            answers_array.append([value[:question_choice_id], value[:text]]) unless QuestionChoice.find(value[:question_choice_id]).blank?
          end
        end

      when "radio_buttons"
        content.first.each do |key,value| 
          answers_array.append([value[:question_choice_id], QuestionChoice.find(value[:question_choice_id]).choice]) unless QuestionChoice.find(value[:question_choice_id]).blank?
        end

      when "check_boxes"
        content.each do |c|
          c.each do |key,value|
            answers_array.append([value[:question_choice_id], QuestionChoice.find(value[:question_choice_id]).choice]) unless value[:question_choice_id].blank? or QuestionChoice.find(value[:question_choice_id]).blank?
          end
        end
      end
    end

    answers_array.each do |record| 
      answers << { 
        'question_choice_id' => record[0], 
        'answer' => record[1],
        'created_at' => Time.now.strftime("%Y-%m-%d %H:%M:%S"),
        'updated_at' => Time.now.strftime("%Y-%m-%d %H:%M:%S") 
        }
    end
                                                       
    return answers
  end
end
