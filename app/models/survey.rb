class Survey < ApplicationRecord
  has_many :question_surveys
  has_many :questions, through: :question_surveys, dependent: :destroy
  has_many :survey_categories
  has_many :categories, through: :survey_categories, dependent: :destroy
  belongs_to :creator

  validates :title, 
    presence: true, 
    length: { maximum: 255 }

  validates :description, 
    presence: true, 
    length: { maximum: 65536 }
  
  validates :public,
    inclusion: { in: [true, false] }
  
  default_scope -> { order(created_at: :desc) }

  def self.search_logged(search,creator)
    if search
      search_formatted = search.downcase.parameterize
      category = Category.joins(:surveys).where("creator_id = ?", creator.id).find_by(name: search_formatted)
      surveys = category.surveys unless category.nil?
      if surveys
        return surveys
      else
        surveys = creator.surveys
      end
    else
      surveys = creator.surveys
    end
  end

  def self.search(search)
    if search
      search_formatted = search.downcase.parameterize
      category = Category.joins(:surveys).where("surveys.public = ?", "True").find_by(name: search_formatted)
      surveys = category.surveys unless category.nil?
      if surveys
        return surveys
      else
        surveys = Survey.all
      end
    else
      surveys = Survey.all
    end
  end
end