module QuestionsHelper
  def question_suggested
    # Query used for autocomplete
    Question.order(:question).joins(:surveys).where("creator_id=?", current_creator.id).map(&:question)
  end
end
