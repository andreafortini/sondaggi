module AnswersHelper
  def question_choice_id_text_type(question)
    question_choice_id = question.question_choices.first.id if question.question_type = "1"
  end

  def render_section(question, index)
    case question.question_type
    when 1
      render partial: "text_field_section", locals: { question: question }
    when 2
      render partial: "radio_button_section", locals: { question: question, index: index }
    when 3
      render partial: "check_box_section", locals: { question: question, index: index }
    end
  end
end
