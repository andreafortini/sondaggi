module SurveysHelper
  def percentage_choice(survey_id, question_choice_id, number_of_answers)
    number_of_answers_per_question_choice = Answer.count_answers_of_question_choice(survey_id, question_choice_id)
    if number_of_answers != 0
      percentage = (number_of_answers_per_question_choice / number_of_answers.to_f) * 100
    else
      percentage = 0
    end
  end

  def answers_text_field(survey_id, question_choice_id)
    answers = Answer.answers_of_text_field(survey_id, question_choice_id)    
  end

  def navigation_link_back(survey)
    if !(current_page?(root_path) or current_page?(surveys_path))
      tag.div class: "mb-3" do
        tag.small { link_to "← Torna indietro", :back }
      end
    end
  end

  def public_or_private
    @survey.public ? "Pubblico" : "Privato"
  end

  def surveys_suggested
    if logged_in? 
      Category.joins(:surveys).where("creator_id = ?", current_creator.id).order(:name).map(&:name)
    else
      Category.joins(:surveys).where("surveys.public = ?", "True").map(&:name)
    end
  end
end
