module QuestionChoicesHelper
  def question_choice_suggested
    # Query used for autocomplete 
    QuestionChoice.order(:choice).joins(questions: :surveys).where("creator_id=?", current_creator.id).map(&:choice)
  end
end
