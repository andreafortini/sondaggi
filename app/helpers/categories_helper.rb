module CategoriesHelper
  def categories_suggested
    Category.order(:name).joins(:surveys).where("creator_id = ?", current_creator.id).map(&:name)
  end
end
