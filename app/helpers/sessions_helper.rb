module SessionsHelper
  def log_in(creator)
    session[:creator_id] = creator.id
  end

  def log_out
    session.delete(:creator_id)
    @current_creator = nil
  end

  def current_creator
    @current_creator ||= Creator.find_by(id: session[:creator_id])
  end

  def logged_in?
    !current_creator.nil?
  end

  def current_creator?(creator)
    creator == current_creator
  end

  def redirect_back_or(default)
    redirect_to(session[:forwarding_url] || default)
    session.delete(:forwarding_url)
  end

  def store_location
    session[:forwarding_url] = request.original_url if request.get?
  end
end
