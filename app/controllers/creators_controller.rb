class CreatorsController < ApplicationController
  before_action :logged_in_creator, only: [:show, :edit, :update]
  before_action :correct_creator, only: [:show, :edit, :update]

  def new
    @creator = Creator.new
  end

  def create
    @creator = Creator.new(creator_params)
    if @creator.save
      log_in @creator
      flash[:success] = "Benvenuto nella Survey app"
      redirect_to @creator
    else
      flash[:danger] = @creator.errors.full_messages[0]
      redirect_to new_creator_path
    end
  end

  def show
    @creator = Creator.find(params[:id])
  end

  def edit
    @creator = Creator.find(params[:id])
  end

  def update
    @creator = Creator.find(params[:id])
    if @creator.update(creator_params)
      flash[:success] = "Modifica effettuata con successo!"
    else
      flash[:danger] = "Qualcosa è andato storto :/"
    end
    redirect_to @creator
  end

  private

  def creator_params
    params.require(:creator).permit(:name, :surname, :email, :password, :password_confirmation)
  end

  def correct_creator
    @creator = Creator.find(params[:id])
    redirect_to(root_url) unless current_creator?(@creator)
  end
end
