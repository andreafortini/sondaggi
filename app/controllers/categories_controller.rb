class CategoriesController < ApplicationController
  before_action :logged_in_creator, only: [:index, :new, :create, :edit, :update, :destroy]
  before_action :get_survey, only: [:index, :new, :create, :edit, :update, :destroy]
  before_action :correct_creator, only: [:index, :new, :create, :edit, :update, :destroy]

  def index
    @categories = @survey.categories
  end

  def new
    @category = @survey.categories.build
  end

  def create
    @category = Category.find_or_create(category_params)
    
    if @category.new_record?
      unless @category.save
        flash[:danger] = "Qualcosa è andato storto :/"
      end
    end

    @category.surveys << Survey.find(params[:survey_id])
    redirect_to survey_categories_path(@survey)
  end

  def edit
    @category = Category.find(params[:id])
  end

  def update
    @category = Category.find(params[:id])
    unless @category.update(category_params)
      flash[:danger] = "Qualcosa è andato storto :/"
    end
    redirect_to survey_categories_path(@survey)
  end

  def destroy
    @category = Category.find(params[:id])
    unless @category.surveys.destroy(@survey)
      flash[:danger] = "Qualcosa è andato storto :/"
    end
    redirect_to survey_categories_path(@survey)
  end

  private 

  def category_params 
    params.require(:category).permit(:name)
  end

  def get_survey
    @survey = Survey.find(params[:survey_id])
  end

  def correct_creator
    redirect_to(root_url) if current_creator != Survey.find(@survey.id).creator
  end
end
