class AnswersController < ApplicationController
  require 'pry'
  before_action :get_answer_detail_references

  def new
  end
  
  def create    
    answers = Answer.set_answers(params[:answers])    

    if Answer.create(answers)
      flash[:success] = "Grazie! Il tuo questionario è stato inviato con successo."
    else
      flash[:danger] = "Qualcosa è andato storto :/"
    end

    redirect_to survey_path(@survey)

  end

  private 

  def answer_params
    params.require(:answers).permit(
      text_fields: [ 
        answer: {} 
      ],
      radio_buttons: [],
      check_boxes: [] 
    )
  end

  def get_answer_detail_references
    @survey = Survey.find(params[:survey_id])
    @questions = @survey.questions
  end
end
