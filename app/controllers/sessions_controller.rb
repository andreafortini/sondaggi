class SessionsController < ApplicationController
  def new
  end

  def create
    creator = Creator.find_by(email: params[:session][:email].downcase)
    if creator && creator.authenticate(params[:session][:password])
      helpers.log_in creator
      redirect_to creator
    else
      flash.now[:danger] = 'email o password sbagliate'
      render 'new'
    end
  end

  def destroy
    helpers.log_out if logged_in?
    redirect_to root_url
  end
end
