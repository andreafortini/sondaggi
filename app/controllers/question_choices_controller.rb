class QuestionChoicesController < ApplicationController
  before_action :logged_in_creator, only: [:index, :new, :create, :edit, :update, :destroy]
  before_action :get_survey_question, only: [:index, :new, :create, :edit, :update, :destroy]
  before_action :correct_creator, only: [:index, :new, :edit, :update, :destroy]

  def index
    @question_choices = @question.question_choices
  end

  def new
    @question_choice = @question.question_choices.new
  end

  def create
    @question_choice = QuestionChoice.find_or_init(question_choice_params, current_creator)

    if @question_choice.new_record?
      unless @question_choice.save
        flash[:danger] = "Qualcosa è andato storto :/"
      end
    end
    @question_choice.questions << Question.find(params[:question_id])
    redirect_to survey_question_question_choices_path(@survey, @question)
  end

  def edit
    @question_choice = QuestionChoice.find(params[:id])
  end

  def update
    @question_choice = QuestionChoice.find(params[:id])
    unless @question_choice.update(question_choice_params)
      flash[:danger] = "Qualcosa è andato storto :/"
    end
    redirect_to survey_question_question_choices_path(@survey, @question)
  end

  def destroy
    @question_choice = QuestionChoice.find(params[:id])
    unless @question_choice.questions.destroy(@question)
      flash[:danger] = "Qualcosa è andato storto :/"
    end
    redirect_to survey_question_question_choices_path(@survey, @question)
  end


  private

  def question_choice_params
    params.require(:question_choice).permit(:choice)
  end

  def get_survey_question
    @survey = Survey.find(params[:survey_id])
    @question = Question.find(params[:question_id])
  end

  def correct_creator
    redirect_to(root_url) if current_creator != @survey.creator
  end
end
