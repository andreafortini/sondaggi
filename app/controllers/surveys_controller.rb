class SurveysController < ApplicationController
  require 'will_paginate/array'
  before_action :logged_in_creator, only: [:new, :create, :edit, :update, :destroy, :statistic]
  before_action :correct_creator, only: [:show, :edit, :update, :destroy, :statistic]

  def index 
    if params[:search].present?
      logged_in? ? @surveys = Survey.search_logged(params[:search],current_creator) : @surveys = Survey.search(params[:search])
    else
      if logged_in?
        @surveys = current_creator.surveys.paginate(page: params[:page], per_page: 15)
      else
        @surveys = Survey.where(public: true).paginate(page: params[:page], per_page: 15)
      end
    end
  end

  def show 
    @survey = Survey.find(params[:id])
  end

  def new
    @survey = current_creator.surveys.new
  end

  def create
    @survey = current_creator.surveys.new(survey_params)
    if @survey.save
      flash[:success] = "Nuovo sondaggio creato con successo!"
      redirect_to @survey
    else
      flash[:danger] = "Qualcosa è andato storto :/"
      redirect_to root_path 
    end
  end

  def edit
    @survey = Survey.find(params[:id])
  end

  def update
    @survey = Survey.find(params[:id])
    if @survey.update(survey_params)
      flash[:success] = "Modifica effettuata con successo!"
    else
      flash[:danger] = @survey.errors.full_messages[0]
    end
    redirect_to @survey
  end

  def destroy
    @survey = Survey.find(params[:id])
    if @survey.destroy
      flash[:success] = "Sondaggio eliminato con successo!"
      redirect_to root_path
    else
      flash[:danger] = "Qualcosa è andato storto :/"
      redirect_to @survey
    end
  end

  def statistic
    @survey = Survey.find(params[:id])
    @questions = @survey.questions

    radio_text_question = Question.question_radio_or_text_of_survey(@survey.id)
    @number_of_answers = Answer.count_answers_of_survey(radio_text_question.first.id)
  end

  private

  def survey_params
    params.require(:survey).permit(:title, :description, :search, :public)
  end

  def correct_creator
    if logged_in?
      redirect_to(root_url) if current_creator != Survey.find(params[:id]).creator
    end
  end
end
