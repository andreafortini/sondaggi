class QuestionsController < ApplicationController
  before_action :logged_in_creator, only: [:new, :create, :edit, :update, :destroy]
  before_action :get_survey, only: [:new, :create, :edit, :update, :destroy]
  before_action :correct_creator, only: [:new, :edit, :update, :destroy]

  def new
    @question = @survey.questions.build
  end

  def create
    @question = Question.find_or_init(question_params, current_creator)

    if @question.new_record?
      if @question.save
        if @question.question_type == 1
          @question_choice = QuestionChoice.create(choice: "")
          @question_choice.questions << Question.find(@question.id)
        end
        flash[:success] = "Domanda aggiunta con successo!"
      else
        flash[:danger] = "Qualcosa è andato storto :/"
      end
    end

    @question.surveys << Survey.find(params[:survey_id])
    redirect_to @survey
  end

  def edit
    @question = Question.find(params[:id])
  end

  def update
    @question = Question.find(params[:id])
    if @question.update(question_params)
      flash[:success] = "Domanda aggiornata con successo!"
    else
      flash[:danger] = "Qualcosa è andato storto :/"
    end
    redirect_to @survey
  end

  def destroy
    @question = Question.find(params[:id])
    if @question.surveys.destroy(@survey)
      flash[:success] = "Domanda eliminata."
    else
      flash[:danger] = "Qualcosa è andato storto :/"
    end
    redirect_to @survey
  end


  private

  def question_params
    params.require(:question).permit(:question, :question_type, :profile_question)
  end

  def get_survey
    @survey = Survey.find(params[:survey_id])
  end

  def correct_creator
    redirect_to(root_url) if current_creator != @survey.creator
  end
end
