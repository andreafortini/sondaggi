class ApplicationController < ActionController::Base
  include SessionsHelper
  

  private

  def logged_in_creator
    unless helpers.logged_in?
      store_location
      flash[:danger] = "È necessario il login"
      redirect_to login_url
    end
  end
end
