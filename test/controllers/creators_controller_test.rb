require 'test_helper'

class CreatorsControllerTest < ActionDispatch::IntegrationTest
  def setup
    @creator = creators(:michael)
    @other_creator = creators(:archer)
  end

  test "should get new" do
    get new_creator_path
    assert_response :success
  end

  test "should get show" do
    log_in_as(@creator)
    get creator_path(@creator)
    assert_response :success
  end

  test "should get edit" do
    log_in_as(@creator)
    get edit_creator_path(@creator)
    assert_response :success
  end

  test "should redirect show when not logged in" do
    get creator_path(@creator)
    assert_not flash.empty?
    assert_redirected_to login_url
  end

  test "should redirect edit when not logged in" do
    get edit_creator_path(@creator)
    assert_not flash.empty?
    assert_redirected_to login_url
  end

  test "should redirect show when logged in as wrong user" do
    log_in_as(@other_creator)
    get creator_path(@creator)
    assert flash.empty?
    assert_redirected_to root_url
  end

  test "should redirect edit when logged in as wrong user" do
    log_in_as(@other_creator)
    get edit_creator_path(@creator)
    assert flash.empty?
    assert_redirected_to root_url
  end

end
