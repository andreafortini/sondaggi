require 'test_helper'

class AnswersControllerTest < ActionDispatch::IntegrationTest
  def setup
    @survey = surveys(:one)
    question_choice = question_choices(:one)
    @answer = question_choice.build_answer(id: 1, answer: "Beautiful")
  end

  test "should get new" do
    get new_survey_answer_path(@survey)
    assert_response :success
  end
end
