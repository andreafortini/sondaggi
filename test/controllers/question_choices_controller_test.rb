require 'test_helper'

class QuestionChoicesControllerTest < ActionDispatch::IntegrationTest
  def setup
    @other_creator = creators(:archer)
    @creator = creators(:michael)
    @survey = surveys(:ad_hoc)
    @question = questions(:first_question_same_survey)
    @question_choice = question_choices(:radio_one)
  end

  test "should get index" do
    log_in_as(@creator)
    get survey_question_question_choices_path(@survey, @question)
    assert_response :success
  end

  test "should get new" do
    log_in_as(@creator)
    get new_survey_question_question_choice_path(@survey, @question)
    assert_response :success
  end

  test "should get edit" do
    log_in_as(@creator)
    get edit_survey_question_question_choice_path(@survey, @question, @question_choice)
    assert_response :success
  end

  test "should not get index without permission" do
    log_in_as(@other_creator)
    get survey_question_question_choices_path(@survey, @question)
    assert_response :redirect
  end

  test "should not get new without permission" do
    log_in_as(@other_creator)
    get new_survey_question_question_choice_path(@survey, @question)
    assert_response :redirect
  end

  test "should not get edit without permission" do
    log_in_as(@other_creator)
    get edit_survey_question_question_choice_path(@survey, @question, @question_choice)
    assert_response :redirect
  end
end
