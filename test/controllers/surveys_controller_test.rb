require 'test_helper'

class SurveysControllerTest < ActionDispatch::IntegrationTest
  
  def setup
    @creator = creators(:michael)
    @other_creator = creators(:archer)
    @survey = surveys(:one)
  end

  test "should get index" do
    get surveys_path
    assert_response :success
  end

  test "sould get show" do
    get survey_path(@survey)
    assert_response :success
  end

  test "should get new" do
    log_in_as(@creator)
    get new_survey_path
    assert_response :success
  end

  test "should get edit" do
    log_in_as(@creator)
    get edit_survey_path(@survey)
    assert_response :success
  end

  test "should not get edit without permission" do
    log_in_as(@other_creator)
    get edit_survey_path(@survey)
    assert_response :redirect
  end

  test "should get statistic" do
    log_in_as(@creator)
    get statistic_survey_path(@survey)
    assert_response :success
  end

  test "should not get statistic without permission" do
    log_in_as(@other_creator)
    get edit_survey_path(@survey)
    assert_response :redirect
  end
end
