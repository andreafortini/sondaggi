require 'test_helper'

class CategoriesControllerTest < ActionDispatch::IntegrationTest
  def setup
    @creator = creators(:michael)
    @survey = surveys(:one)
    @category = categories(:one)   
  end

  test "should get index" do
    log_in_as(@creator)
    get survey_categories_path(@survey)
    assert_response :success
  end

  test "should get new" do
    log_in_as(@creator)
    get new_survey_path(@survey)
    assert_response :success
  end

  test "should get edit" do
    log_in_as(@creator)
    get edit_survey_category_path(@survey, @category)
    assert_response :success
  end
end
