require 'test_helper'

class QuestionsControllerTest < ActionDispatch::IntegrationTest
  def setup 
    @other_creator = creators(:archer)
    @creator = creators(:michael)
    @survey = surveys(:one)
    @question = questions(:one)
  end

  test "should get new" do
    log_in_as(@creator)
    get new_survey_question_path(@survey)
    assert_response :success
  end

  test "sould get edit" do
    log_in_as(@creator)
    get edit_survey_question_path(@survey, @question)
    assert_response :success
  end

  test "should not get new without permission" do
    log_in_as(@other_creator)
    get new_survey_question_path(@survey)
    assert_response :redirect
  end

  test "should not get edit without permission" do
    log_in_as(@other_creator)
    get edit_survey_question_path(@survey, @question)
    assert_response :redirect
  end
end