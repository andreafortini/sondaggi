require 'test_helper'

class CreatorEditTest < ActionDispatch::IntegrationTest
  def setup
    @creator = creators(:michael)
  end

  test "should redirect update when not logged in" do
    patch creator_path(@creator), 
      params: { 
        creator: { 
          name: "Andrea", 
          email: "example@mail.it", 
          password: "foooooo", 
          password_confirmation: "foooooo" 
        } 
      }
    assert_redirected_to login_url
  end

  test "unsuccessful edit" do
    log_in_as(@creator)
    get edit_creator_path(@creator)
    assert_response :success

    patch creator_path(@creator), 
      params: { 
        creator: { 
          name: "", 
          email: "invalid@mail", 
          password: "foooooo", 
          password_confirmation: "wrongpassword" 
        } 
      }

    assert_not_equal "Modifica effettuata con successo!", flash[:success]
    assert_equal "Qualcosa è andato storto :/", flash[:danger]                                                 
    assert_redirected_to creator_path(@creator)
  end
end
