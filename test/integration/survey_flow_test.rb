require 'test_helper'

class SurveyFlowTest < ActionDispatch::IntegrationTest
  def setup
    @creator = creators(:michael)
    @creator_guest = creators(:archer)
    @survey = surveys(:ad_hoc)
    @survey_guest = surveys(:two)
    @survey_private = surveys(:private)
    @questions = @survey.questions
    @question = questions(:second_question_same_survey)
  end

  test "display survey home page" do
    get root_path
    assert_response :success

    assert_not is_logged_in?
    assert_select "h1", "Sondaggi"
    assert_select "a[href=?]", survey_path(@survey), count: 1
    assert_select "a[href=?]", survey_path(@survey_private), count: 0
    assert_select "a[href=?]", root_url, count: 0
    assert_select "a[href=?]", login_path
  end

  test "should not display other creators' surveys when creator is logged" do
    log_in_as(@creator)
    get root_path

    assert is_logged_in?
    assert_select "a[href=?]", survey_path(@survey)
    assert_select "a[href=?]", survey_path(@survey_guest), count: 0
  end

  test "display show survey not logged" do
    get survey_path(@survey)
    assert_response :success

    assert_not is_logged_in?
    assert_select "a[href=?]", new_survey_answer_path(@survey)
  end

  test "display show survey logged" do
    log_in_as(@creator)
    get survey_path(@survey)
    assert_response :success
    assert is_logged_in?

    assert_select "a[href=?]", new_survey_question_path(@survey) # add question
    assert_select "a[href=?]", edit_survey_path(@survey) # edit survey
    assert_select "a[href=?]", survey_path(@survey.id) # delete survey
    assert_select "a[href=?]", survey_question_question_choices_path(@survey, @question) # details question
    assert_select "a[href=?]", edit_survey_question_path(@survey, @question) # edit question
    assert_select "a[href=?]", survey_question_path(@survey.id, @question.id) # delete question
    assert_select "a[href=?]", statistic_survey_path(@survey) # view statistics
  end

  test "should redirect create when not logged in" do
    assert_no_difference 'Survey.count' do
      post surveys_path, params: { survey: { title: "Title", description: "Lore Ipsum" } }
    end
    assert_redirected_to login_url
  end

  test "should redirect update when not logged in" do
    patch survey_path(@survey), params: { survey: { title: "Title", description: "Lore Ipsum" } }
    assert_redirected_to login_url
  end

  test "should redirect destroy when not logged in" do
    assert_no_difference 'Survey.count', -1 do
      delete survey_path(@survey.id)
    end
    assert_redirected_to login_url
  end

  test "new survey flow from wrong to right insert" do
    log_in_as(@creator)
    get new_survey_path(@survey)
    assert_response :success
    assert is_logged_in?
    
    assert_select 'input', count: 4

    # Wrong insert
    post surveys_path, params: { survey: { title: "", description: "", public: true } }

    # Right insert
    assert_difference 'Survey.count', 1 do
      post surveys_path, params: { survey: { title: "Title", description: "Lore Ipsum", public: true } }
    end
    assert_not flash[:success].nil?
  end

  test "should delete survey" do
    log_in_as(@creator)
    get survey_path(@survey)
    assert_response :success
    assert is_logged_in?

    assert_difference 'Survey.count', -1 do
      delete survey_path(@survey.id)
    end
    assert_not flash[:success].nil?
  end
end
