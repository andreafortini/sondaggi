require 'test_helper'

class CreatorsSignupTest < ActionDispatch::IntegrationTest
  test "bad signup test" do
    get new_creator_path

    assert_no_difference "Creator.count" do
      post creators_path, 
        params: { 
          creator: { 
            name: "",
            surname: "", 
            email: "invalid@mail", 
            password: "foooooo", 
            password_confirmation: "wrongpassword" 
          } 
        }
    
    assert_redirected_to new_creator_path
    end
  end

  test "good signup test" do
    get new_creator_path

    assert_difference "Creator.count" do
      post creators_path, 
        params: { 
          creator: { 
            name: "valid", 
            surname: "valid",
            email: "valid@mail.it", 
            password: "validpassword", 
            password_confirmation: "validpassword" 
          } 
        }
    end
  end
end
