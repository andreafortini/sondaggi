require 'test_helper'

class QuestionFlowTest < ActionDispatch::IntegrationTest
  def setup
    @other_survey = surveys(:two)
    @creator = creators(:michael)
    @survey = surveys(:ad_hoc)
    @question_text = questions(:second_question_same_survey)
    @question_radio = questions(:second_question_same_survey)
  end

  test "should not create question if not logged in" do
    assert_no_difference 'Question.count' do
      post survey_questions_path(@survey), params: { question: { question: "Test question", question_type: 1, profile_question: false } }
    end
    assert_redirected_to login_url
  end

  test "new question flow" do
    log_in_as(@creator)
    get new_survey_question_path(@survey)
    assert :success

    assert_select "input[type=?]", "text", count: 1
    assert_select "input[type=?]", "radio", count: 3
    assert_select "input[type=?]", "checkbox", count: 1
    
    # Wrong insert
    assert_no_difference 'Question.count' do
      post survey_questions_path(@survey), params: { question: { question: "", question_type: 1, profile_question: false } }
    end
    assert flash[:success].nil?

    # try to access inside another creator area
    new_survey_question_path(@other_survey)
    assert_response :redirect

    # Right insert
    assert_difference 'Question.count' do
      post survey_questions_path(@survey), params: { question: { question: "Test question", question_type: 1, profile_question: false } }
    end
    assert_not flash[:success].nil?
    assert_redirected_to @survey
  end

  test "should also insert question choice when question type is one" do
    log_in_as(@creator)
    get new_survey_question_path(@survey)
    assert :success

    assert_difference 'QuestionChoice.count' do
      post survey_questions_path(@survey), params: { question: { question: "Test question", question_type: 1, profile_question: false } }
    end
    assert_not flash[:success].nil?
    assert_redirected_to @survey

    assert_no_difference 'QuestionChoice.count' do
      post survey_questions_path(@survey), params: { question: { question: "Test question", question_type: 2, profile_question: false } }
    end
    assert_not flash[:success].nil?
    assert_redirected_to @survey
  end

  test "insert question if not created yet or reuse the existing one" do
    log_in_as(@creator)
    get new_survey_question_path(@survey)
    assert :success

    question = "Magic question"
    q_type = 1
    q_profile = false

    # First attempt
    assert_difference 'Question.count' do
      post survey_questions_path(@survey), params: { question: { question: question, question_type: q_type, profile_question: q_profile } }
    end
    assert_not flash[:success].nil?
    assert_redirected_to @survey

    survey_two = surveys(:one)
    get new_survey_question_path(survey_two)
    assert :success

    # Second attempt 
    assert_no_difference 'Question.count' do
      post survey_questions_path(survey_two), params: { question: { question: question, question_type: q_type, profile_question: q_profile } }
    end
  end
end
