require 'test_helper'

class CreatorLoginTest < ActionDispatch::IntegrationTest
  def setup 
    @creator = creators(:michael)
  end

  test "login with invalid information" do
     get login_path
     assert_response :success

     post login_path, params: { session: { email: "", password: "" } }
     assert_not flash.empty?

     get login_path
     assert flash.empty?
  end

  test "login with valid information followed by logout" do
    #login

    get login_path
    assert_response :success

    post login_path, params: { session: { email: @creator.email, password: 'password' } }
    assert is_logged_in?
    assert_redirected_to creator_url(@creator)
    follow_redirect!

    assert_select "a[href=?]", login_path, count: 0
    assert_select "a[href=?]", logout_path

    # logout 

    delete logout_path
    assert_not is_logged_in?
    assert_redirected_to root_path
    # Simulate a user clicking logout in a second window.
    delete logout_path

    follow_redirect!
    assert_select "a[href=?]", login_path
  end
end
