require 'test_helper'

class SurveyTest < ActiveSupport::TestCase
  
  def setup
    @survey = surveys(:one)
  end

  test "should be valid" do
    assert @survey.valid?
  end

  test "order should be most recent first" do
    assert_equal Survey.first, surveys(:most_recent)
  end

  # title

  test "title should be present" do
    @survey.title = " "
    assert_not @survey.valid?
  end

  test "title should not be too long" do
    @survey.title = "a" * 256
    assert_not @survey.valid?
  end

  # description

  test "description should be present" do
    @survey.description = " "
    assert_not @survey.valid?
  end

  test "description should not be too long" do
    @survey.title = "a" * 65537
    assert_not @survey.valid?
  end

  # public

  test "public shold be present" do
    @survey.public = nil
    assert_not @survey.valid?
  end
  
  # creator_id

  test "creator id shoud be present" do 
    @survey.creator_id = nil
    assert_not @survey.valid?
  end

end
