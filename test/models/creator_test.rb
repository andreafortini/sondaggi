require 'test_helper'

class CreatorTest < ActiveSupport::TestCase
  
  def setup
    @creator = Creator.new(email: "andreafortini@unife.it", name: "Andrea", surname: "Fortini", 
                        password: "admin1", password_confirmation: "admin1")
  end

  test "should be valid" do 
    assert @creator.valid?
  end

  # email

  test "email should be present" do
    @creator.email = " "
    assert_not @creator.valid?
  end

  test "email should not be too long" do
    @creator.email = "a" * 246 + "@unife.com"
    assert_not @creator.valid?
  end

  test "email validation should accept valid addresses" do
    valid_addresses = %w[creator@unife.com CREATOR@foo.COM A_US-ER@foo.bar.org first.last@foo.jp andrea+fortini@baz.cn]
    valid_addresses.each do |valid_address|
      @creator.email = valid_address
      assert @creator.valid?, "#{valid_address.inspect} should be valid"
    end
  end

  test "email validation should reject invalid addresses" do
    invalid_addresses = %w[creator@unife,com creator_at_foo.org creator.name@unife. foo@bar_baz.com foo@bar+baz.com]
    invalid_addresses.each do |invalid_address|
      @creator.email = invalid_address
      assert_not @creator.valid?, "#{invalid_address.inspect} should be invalid"
    end
  end

  test "email addresses should be unique" do
    duplicate_creator = @creator.dup
    duplicate_creator.email = @creator.email.upcase
    @creator.save
    assert_not duplicate_creator.valid?
  end
  
  test "should be saved as all lower-case" do
    mix_case = "AnDrEa@UniFE.COM"
    @creator.email = mix_case
    @creator.save
    assert_equal @creator.reload.email, mix_case.downcase
  end

  # name

  test "name should be present" do
    @creator.name = " "
    assert_not @creator.valid?
  end

  test "name should not be too long" do
    @creator.name = "a" * 51
    assert_not @creator.valid?
  end

  # surname

  test "surname should be present" do
    @creator.surname = " "
    assert_not @creator.valid?
  end

  test "surname should not be too long" do
    @creator.surname = "a" * 51
    assert_not @creator.valid?
  end

  # password

  test "password should have a minimum length" do
    @creator.password = @creator.password_confirmation = "a" * 5
    assert_not @creator.valid?
  end

end
