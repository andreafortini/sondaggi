require 'test_helper'

class QuestionChoiceQuestionTest < ActiveSupport::TestCase
  def setup
    question_choice = question_choices(:one)
    question = questions(:one)
    @question_choice_question = QuestionChoiceQuestion.new(question_id: question.id, question_choice_id: question_choice.id)
  end

  test "should be valid" do
    assert @question_choice_question.valid?
  end

  test "should require a question_id" do
    @question_choice_question.question_id = nil
    assert_not @question_choice_question.valid?
  end

  test "should require a question_choice_id" do
    @question_choice_question.question_choice_id = nil
    assert_not @question_choice_question.valid?
  end
end
