require 'test_helper'

class QuestionSurveyTest < ActiveSupport::TestCase
  def setup
    survey = surveys(:one)
    question = questions(:one)
    @question_survey = QuestionSurvey.new(survey_id: survey.id, question_id: question.id)
  end

  test "should be valid" do
    assert @question_survey.valid?
  end

  test "should require a survey_id" do
    @question_survey.survey_id = nil
    assert_not @question_survey.valid?
  end

  test "should require a question_id" do
    @question_survey.question_id = nil
    assert_not @question_survey.valid?
  end
end
