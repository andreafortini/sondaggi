require 'test_helper'

class QuestionChoiceTest < ActiveSupport::TestCase
  
  def setup
    @question_choice = QuestionChoice.new(choice: "This choice is a test")
  end

  test "should be valid" do
    assert @question_choice.valid?
  end

  # choice

  test "choice should not be too long" do
    @question_choice.choice = "a" * 141
    assert_not @question_choice.valid?
  end

  # other associations

  test "associated answer should be destroyed" do 
    @question_choice.save
    @question_choice.create_answer!(answer: @question_choice.choice)
    assert_difference 'Answer.count', -1 do
      @question_choice.destroy
    end
  end

end
