require 'test_helper'

class AnswerTest < ActiveSupport::TestCase
  def setup
    question_choice = question_choices(:one)
    @answer = question_choice.build_answer(answer: "Beautiful")
  end

  test "should be valid" do
    assert @answer.valid?
  end

  # question_choice_id

  test "question choice id should be present" do
    @answer.question_choice_id = nil
    assert_not @answer.valid?
  end

  # answer

  test "answer should not be too long" do
    @answer.answer = "a" * 141
    assert_not @answer.valid?
  end

  test "answer should be present" do
    @answer.answer = " "
    assert_not @answer.valid?
  end 
end
