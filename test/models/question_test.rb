require 'test_helper'

class QuestionTest < ActiveSupport::TestCase

  def setup
    @question = Question.new(question: "What is your favourite color?", question_type: 1, profile_question: false)
  end

  test "should be valid" do
    assert @question.valid? 
  end

  # question

  test "question should be present" do
    @question.question = " "
    assert_not @question.valid?
  end

  test "question should not be too long" do
    @question.question = "a" * 256
    assert_not @question.valid?
  end

  # question_type

  test "question type should be present" do
    @question.question_type = nil
    assert_not @question.valid?
  end

  test "question type should be one or two or three" do
    @question.question_type = 4
    assert_not_includes( [1,2,3], @question.question_type)
  end

  # profile_question

  test "profile question should be present" do
    @question.profile_question = nil
    assert_not @question.valid?
  end

end
