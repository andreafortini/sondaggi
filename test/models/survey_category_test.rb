require 'test_helper'

class SurveyCategoryTest < ActiveSupport::TestCase
  def setup
    survey = surveys(:one)
    category = categories(:one)
    @survey_category = SurveyCategory.new(survey_id: survey.id, category_id: category.id)
  end

  test "should be valid" do
    assert @survey_category.valid?
  end

  test "should require a survey_id" do
    @survey_category.survey_id = nil
    assert_not @survey_category.valid?
  end

  test "should require a category_id" do
    @survey_category.category_id = nil
    assert_not @survey_category.valid?
  end
end
