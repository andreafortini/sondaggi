require 'test_helper'

class CategoryTest < ActiveSupport::TestCase
  def setup
    @category = Category.new(name: "Category")    
  end

  test "should be valid" do
    assert @category.valid?
  end

  # name

  test "name should be present" do
    @category.name = " "
    assert_not @category.valid?
  end

  test "name should not be too long" do
    @category.name = "a" * 21
    assert_not @category.valid?
  end  

  test "should be saved as all lower-case" do
    name = "UpperCategory"
    @category.name = name
    @category.save
    assert_equal @category.reload.name, name.downcase
  end

  test "name should be unique" do
    duplicate_category = @category.dup
    duplicate_category.name = @category.name.upcase
    @category.save
    assert_not duplicate_category.valid?
  end

  test "spaces should be replaced with dash" do
    name = "with space"
    @category.name = name
    @category.save
    assert_equal @category.reload.name, name.parameterize
  end
end
