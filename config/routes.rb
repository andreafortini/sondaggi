Rails.application.routes.draw do
  root 'surveys#index'
  
  get    'login'   => 'sessions#new'
  post   'login'   => 'sessions#create'
  delete 'logout'  => 'sessions#destroy'
  
  resources :creators, only: [:new, :create, :show, :edit, :update]

  resources :surveys do

    get 'statistic', on: :member

    resources :answers, only: [:new, :create, :edit, :update, :destroy]
    resources :categories, only: [:index, :new, :create, :edit, :update, :destroy]
    resources :questions, only: [:new, :create, :edit, :update, :destroy] do
      resources :question_choices, only: [:index, :new, :create, :edit, :update, :destroy]
    end

  end

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
